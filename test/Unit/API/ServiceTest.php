<?php
namespace Komponente4\API;

/**
 * Created by PhpStorm.
 * User: michaelsandritter
 * Date: 3/09/2015
 * Time: 2:55 PM
 */

class ServiceTest extends \PHPUnit_Framework_TestCase{


    private $service;

    /**
     *
     */
    public function setUp()
    {
        $this->service = new Service();
    }

    /**
     * @test
     */
    public function shouldReturnContent()
    {
        $this->assertEquals('component 4', $this->service->getContent());
    }

}